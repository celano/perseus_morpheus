declare variable $t:=
db:open("hib_parses");

declare variable $u :=
db:open("hib_lemmas");

for $j in $t//row,
    $nn in $u//row
    (: this restricts the search to Latin only:) 
    [./field[@name="lemma_lang_id"]/text() = "3"] 
    (: this is the join :)
    [(# db:enforceindex #) {./field[@name="lemma_id"]/text() = $j/field[@name="lemma_id"]/text() }]


return
(# db:copynode false #) {
element
wf 
{
<f>{$j/*}</f>,<l>{$nn/*}</l>  
}
}
