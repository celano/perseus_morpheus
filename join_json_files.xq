declare variable $u :=
parse-json(file:read-text("hib_parses.json")) => array:flatten();

declare variable $o :=
parse-json(file:read-text("hib_lemmas.json")) => array:flatten();

for $i in $u,
    $ll in $o[?lemma_id = $i?lemma_id]
return
map:merge(($i, $ll))