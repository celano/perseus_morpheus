# Morpheus

This repository contains Morpheus, the Perseus morphological anlyzer. I make available, in XML and JSON format, the sql tables which can be downloaded at:

https://www.perseus.tufts.edu/hopper/opensource/download